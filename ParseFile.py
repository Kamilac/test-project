import csv, json
class ParseCsv:
    def __init__(self, file='Dupa'):
        self._file_ = file

    def get_as_list(self):
        with open(self._file_) as csv_file:
            csv_reader = csv.DictReader(csv_file)
            data = list()
            for row in csv_reader:
                data.append(row)
            return data


class ReadJson:
    def __init__(self, list: [dict]):
        self.list = list

    def set_file(self, file):
        with open(file, 'w') as jsonFile:
            jsonFile.write(json.dumps(self.list, indent=4).replace(' ', ''))


print(str(ParseCsv("Faith.csv").get_as_list()))

lista_i = ParseCsv("Faith.csv").get_as_list()

for el in lista_i:
    keys = el.keys()
    for gs in range(0, len(keys)):
        print(f"{list(el.keys())[gs]} : {list(el.values())[gs]}")

ReadJson(ParseCsv("Faith.csv").get_as_list()).set_file("Json.json")
ReadJson(ParseCsv("Faith.csv").get_as_list()).set_file("Json.json")
ReadJson(ParseCsv("Faith.csv").get_as_list()).set_file("Json.json")
ReadJson(ParseCsv("Faith.csv").get_as_list()).set_file("Json.json")
ReadJson(ParseCsv("Faith.csv").get_as_list()).set_file("Json.json")

